<?php

require('animal.php');
require('frog.php');
require('ape.php');

$sheep = new Animal("shaun");
echo "name : " . $sheep->name . "<br>";
echo "legs = " . $sheep->legs . "<br>";
echo "cold blooded : " . $sheep -> cold_blooded . "<br>";
echo "<br>";

$frog = new Frog("buduk");
echo "name : " . $frog->name . "<br>";
echo "legs = " . $frog->legs . "<br>";
echo "cold blooded : " . $frog -> cold_blooded . "<br>";
echo "Jump : " . $frog->jump . "<br>";
echo "<br>";

$sungokong = new Ape("kera sakti");
echo "name : " . $sungokong->name . "<br>";
echo "legs : " . $sungokong->legs . "<br>";
echo "cold blooded : " . $sungokong -> cold_blooded . "<br>";
echo "Yell : ". $sungokong->yell ."<br>";
echo "<br>";






?>